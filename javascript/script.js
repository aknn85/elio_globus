var script = document.createElement('script');
script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js';
script.type = 'text/javascript';
document.getElementsByTagName('head')[0].appendChild(script);

var numimg = 8;
var curimg = 1;
var priceWeight = 2.10;
var priceWeightRabt = 2.60;
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("bigShopViewImageSlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}

function  goback() {
    var getClassID = document.getElementById("anotherProductInteresseSlideContainer");
    var imgs = getClassID.getElementsByTagName("img");
    if(this.curimg>0)
   {
    var imageEle = imgs[this.curimg-1]
    this.curimg = this.curimg - 1;
    var slideBlock = imageEle.offsetWidth;
    $('#anotherProductInteresseSlideContainer').animate({scrollLeft:'-='+slideBlock}, 1000, "swing");
   }

}
function gofwd(){
    var getClassID = document.getElementById("anotherProductInteresseSlideContainer");
    var imgs = getClassID.getElementsByTagName("img");
        if(this.curimg < (imgs.length - 1)){
        var imageEle = imgs[this.curimg+1]
        this.curimg = this.curimg + 1;
        var slideBlock = imageEle.offsetWidth;
        $('#anotherProductInteresseSlideContainer').animate({scrollLeft:'+='+slideBlock}, 1000, "swing");
       }
}
function funcAllProduct(){
    var allProduct = document.getElementById("allProductWindow").style.visibility
    if(allProduct == "hidden"){
        $('#allProductWindow').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0}, 700);
        document.getElementById("allProductIcon").className = "fas fa-sort-up"
    }else{
        $('#allProductWindow').css({opacity: 1.0, visibility: "hidden"}).animate({opacity: 0}, 700);
        document.getElementById("allProductIcon").className = "fas fa-sort-down"
    }
}
function plusMinusAmount(n){
    //var inputText = inputConvert(input);
    var input = parseInt(document.getElementById("plusMinusAmountID").value);
    var endInput = (n + input);
    if ((parseInt(n) == -1) && (input > 1)){
        
        textFieldOutputPrice(endInput);

        if (n + input == 1){
            document.getElementById("minusID").disabled = true;
        }else if (n + input > 1){
            document.getElementById("plusID").disabled = false;
        }
    }else if ((parseInt(n) == 1) && (input < 100)){

        textFieldOutputPrice(endInput);

        if (n + input == 100){
            document.getElementById("plusID").disabled = true;
        }else if (n + input < 100){
            document.getElementById("minusID").disabled = false;
        }
    } else if(input > 100){
        textFieldOutputPrice(100);
    }
}
function textFieldOutputPrice(endInput){
    document.getElementById("plusMinusAmountID").value = endInput;
    document.getElementById("FixedCostLabelID").innerHTML = String((endInput*this.priceWeight).toFixed(2)) + " €"
    document.getElementById("FixedCostRabtID").innerHTML = String((endInput*this.priceWeightRabt).toFixed(2)) + " €"
}

function buttonWeightPrice(n){
    if (n == 500){
        this.priceWeight = 3.50;
        this.priceWeightRabt = 4.3;
        document.getElementById("FixedCostLabelID").value = String(3.50) + " €"
        document.getElementById("FixedCostRabtID").value = String(4.3) + " €";
    }else if(n == 300){
        this.priceWeight = 2.80;
        this.priceWeightRabt = 3.46;
        document.getElementById("FixedCostLabelID").value = String(2.80) + " €"
        document.getElementById("FixedCostRabtID").value = String(3.46) + " €";
    }else{
        this.priceWeight = 2.10;
        this.priceWeightRabt = 2.60;
        document.getElementById("FixedCostLabelID").value = String(2.10) + " €"
        document.getElementById("FixedCostRabtID").value = String(2.60) + " €";
    }
    var input = parseInt(document.getElementById("plusMinusAmountID").value);
    textFieldOutputPrice(input);
}

function inputConvert(e){
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
    // Allow: Ctrl+A
    (e.keyCode == 65 && e.ctrlKey === true) || 
    // Allow: home, end, left, right
    (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
    }
    return e;
}

$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        
    });